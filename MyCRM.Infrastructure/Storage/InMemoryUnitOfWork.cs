﻿using System.Collections.Generic;
using System.Linq;

namespace MyCRM.Infrastructure.Storage
{
	public class InMemoryUnitOfWork : IEventStoreUnitOfWork
	{
		private readonly IList<object> _data = new List<object>();

		public IQueryable<T> Query<T>() where T:class, new()
		{
			return Data.OfType<T>().AsQueryable();
		}

		public void Delete<T>(T entity) where T : class, IEntity, new()
		{
			Data.Remove(entity);
		}

		public void Delete(object entity)
		{
			Data.Remove(entity);
		}

		public void Store<T>(T entity) where T : class, IEntity, new()
		{
			Store((object)entity);
		}

		public void Store(object entity)
		{
			Data.Add(entity);
		}

		public void SaveChanges() { }

		public void Dispose() { }

		public IList<object> Data
		{
			get { return _data; }
		}
	}
}
