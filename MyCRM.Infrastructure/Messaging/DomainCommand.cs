﻿using System;

namespace MyCRM.Infrastructure.Messaging
{
    public abstract class DomainCommand : IMessage
    {
        public Guid AggregateId { get; set; }
    }
}