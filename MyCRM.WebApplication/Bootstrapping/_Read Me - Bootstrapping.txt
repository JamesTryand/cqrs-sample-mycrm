﻿Insert here code to register your components in DI-Container (I use structuremap. Feel free to change it in your favorite DI-Container)

Current it's registered:
- EventStore for Domain and Aggregates
- ReadModelRepository for ReadModel
- Messagebus for Messagetransfer (Commands from Controllers to Domain and Events from Domain to ReadModel)

Bootstrapper is able to autoregister Commands to CommandHandler and Events to EventHandler